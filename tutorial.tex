\documentclass[11pt,a4paper]{article}

\usepackage{polski}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english,polish]{babel}

\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{listings,lstautogobble}

\usepackage{indentfirst}

\begin{document}
    \lstset{language=bash,
            autogobble=true,
            literate = {-}{-}1}

    \title{Podstawy systemu git}
    \author{Marcin Moździerz}
    \date{\today}
    \maketitle
    
    \begin{center}
        \footnotesize
        Najnowsza wersja zawsze dostępna na \url{https://bitbucket.org/mozdzi/git-tutorial/src}
    \end{center}
    
    
    \section{Wprowadzenie}
        \subsection*{Motywacja}
            Kontrola wersji to mniej lub bardziej zorganizowany sposób śledzenia zmian w plikach. Ma to wiele zalet, między innymi możliwość przywrócenia historycznych wersji, co pozwala np. dowolnie eksperymentować z kodami źródłowymi programów bez obawy ich nieodwracalnego uszkodzenia. Poza tym ułatwia to pracę w grupie - możemy porównać ostatnią wersję pliku przechowywanego przez nas z plikiem zmodyfikowanym przez współpracownika i łatwo zorientować się, co zostało zmienione. Jeżeli regularnie przesyłamy pliki na serwer, każdy ma dostęp do najnowszej wersji. \par
            Prawdopodobnie większość osób pracujących na komputerze korzystała kiedyś z \textit{jakiegoś} rodzaju kontroli wersji. Najprostszy sposób to skopiowanie plików, nad którymi aktualnie pracujemy do nowego folderu, co sprawia znaczne problemy z utrzymaniem porządku na dysku czy może prowadzić do przywrócenia nie tej wersji pliku, co chcemy. Bardziej finezyjna metoda polega na przechowywaniu danych w chmurach, np. Dropbox czy MEGAsync, które pozwalają przywracać poprzednie wersje plików, jednak zazwyczaj również kończy to się bałaganem i historią składającą się bardzo szybko z tysięcy zapisów pojedynczego pliku\footnote{Chmury domyślnie synchronizują się po każdym zapisie pliku.}. \par
            Oczywiście zakładanie setek folderów jest niezbyt wygodne, dlatego istnieje specjalne oprogramowanie pozwalające kontrolować wersje w sposób usystematyzowany. Pośród kilku modeli kontroli wersji najbardziej zaawansowane i najwygodniejsze w użyciu są rozproszone systemy kontroli wersji (DVCS - Distributed Version Control System). Najpopularniejszym systemem DVCS jest git, napisany przez Linusa Torvardsa, którego znajomość jest obecnie standardem w przemyśle IT\footnote{\href{https://github.com/business/customers}{Tutaj} na przykład możemy znaleźć firmy IT hostujące swoje projekty na GitHubie.}.
            
        \subsection*{Jak to działa?}
            DVCS składają się z centralnego serwera oraz klientów. Historia wersji oraz kopie plików - czyli tak zwane \textit{repozytorium} - są przechowywane na serwerze, a także lokalnie na dyskach klientów (Rys. \ref{Fig:dvcs}). Dzięki temu jeżeli awarii sprzętowej ulegnie centralny serwer lub któryś z klientów, to mimo wszystko łatwo można przywrócić całą historię wersji. Jak widać, łatwo też pracować na kilku komputerach jednocześnie bez polegania na synchronizacji chmury.
            
            \begin{figure}[h]
                \centering
                \includegraphics[width=0.7\textwidth]{img/dvcs.png}
                \caption{Rozproszony system kontroli wersji}
                \label{Fig:dvcs}
            \end{figure}
            
            Cechą specyficzną gita jest to, że historia wersji jest przechowywana jako zestaw migawek (snapshots), które pokazują, jak wyglądają \textbf{wszystkie} pliki w danym momencie\footnote{Większość systemów kontroli wersji przechowuje tylko pierwotny plik oraz tzw. pliki różnicowe, pokazujące, co zmieniło się w stosunku do pierwszej wersji.}. Jeżeli dany plik nie został zmieniony, przechowywana jest tylko referencja do wersji z poprzedniej migawki (Rys. \ref{Fig:snapshots}).
            
            \begin{figure}[h]
                \centering
                \includegraphics[width=0.7\textwidth]{img/snapshots.png}
                \caption{Migawki}
                \label{Fig:snapshots}
            \end{figure}
            
            Operacje, np. przejrzenie historii wersji, porównanie obecnego pliku ze starą wersją czy jej przywrócenie, zapisanie wykonanych zmian w repozytorium - tzw. \textit{commit} - wykonywane są lokalnie, co umożliwia pracę z gitem bez dostępu do internetu. Oczywiście warto co jakiś czas przesyłać zmiany na serwer (operacja taka nazywa się \textit{push}), szczególnie pracując grupowo. \par
            Przechowywane dane mają obliczaną sumę kontrolną za pomocą funkcji skrótu SHA-1, co zabezpiecza informacje przed błędami bądź modyfikacją z zewnątrz.
    
    
    \section{Instalacja gita}
        Git można używać tylko lokalnie na swoim komputerze bądź na serwerze. Jeżeli mielibyśmy swój serwer, to moglibyśmy tam zainstalować oprogramowanie. Jednak ponieważ nie mamy serwera, a także ponieważ jeszcze lepiej zabezpieczymy się przed awarią sprzętu, wygodnie będzie użyć serwisu hostingowego (\textit{git server}). \par
        Najbardziej znane serwisy to \href{https://github.com/}{GitHub} oraz \href{https://bitbucket.org/}{Bitbucket}. GitHub jest co prawda najpopularniejszy (zobacz przypis 2), ale za darmo można założyć tylko repozytoria publiczne. Bitbucket natomiast umożliwia założenie dowolnej ilości prywatnych repozytoriów, przy czym z jednego takiego repo może korzystać maksymalnie 5 osób. Myślę, że w większości wypadków to powinno nam wystarczyć, dlatego proponuję \href{https://bitbucket.org/}{Bitbucket}. \par
        Po założeniu konta na Bitbucket można przystąpić do instalacji gita.
        
        \begin{itemize}
            \item \textbf{Instalacja dla Windows}
                \begin{enumerate}
                    \item Trzeba zainstalować program Git for Windows. Najlepiej zrobić to od razu z credential managerem, by uniknąć wpisywania nazwy użytkownika i hasła za każdym razem. Instalator dostępny pod adresem \url{https://github.com/Microsoft/Git-Credential-Manager-for-Windows/releases/latest}.
                    \item Podczas instalacji zostawmy opcje domyślne.
                    \item Proszę sprawdzić w menu start czy jest i działa program Git->Git Bash (oczywiście jeżeli zdecydowałeś się na utworzenie folderu w menu start).
                    \item Jeżeli jednak tak się nie stało (albo jak nie chcesz korzystać z credential managera), to proszę skorzystać z instalatora dostępnego pod linkiem \url{https://git-for-windows.github.io/}.
                \end{enumerate}
                
            \item \textbf{Instalacja dla Mac OS X}
                \begin{enumerate}
                    \item Należy skorzystać z najnowszej wersji instalatora ze strony \url{https://sourceforge.net/projects/git-osx-installer/files/}.
                    \item Instalację można zweryfikować w terminalu za pomocą polecenia:
                    \begin{lstlisting}
                        $ git --version
                    \end{lstlisting}
                    \item Aby uniknąć wpisywania nazwy użytkownika i hasła za każdym razem, można skorzystać z programu git-credential-osxkeychain; umożliwia to przechowywanie haseł w keychainie maca. Program powinien być zainstalowany razem z gitem; można to sprawdzić poleceniem:
                    \begin{lstlisting}
                        $ git credential-osxkeychain
                    \end{lstlisting}
                    Żeby udostępnić funkcjonalność git-credential-osxkeychain wystarczy w terminalu użyć polecenia:
                    \begin{lstlisting}
                        $ git config --global credential.helper osxkeychain
                    \end{lstlisting}
                \end{enumerate}
                
            \item \textbf{Konfiguracja}
                \begin{enumerate}
                    \item Konfiguracja polega na sprecyzowaniu naszych danych, które będą powiązane z każdym commitem. Robi to się za pomocą programu Git Bash (Windows) / konsoli (Mac OS X). W moim przypadku wygląda to tak:
                    \begin{lstlisting}
                        $ git config --global user.name ''Marcin Mozdzierz''
                        $ git config --global user.email ''marcinm@agh.edu.pl''
                    \end{lstlisting}
                \end{enumerate}
        \end{itemize}
        
        Dalszą pracę należy wykonywać w Git Bash (Windows)\footnote{Używa się tego tak, jak terminala systemów unixowych.} / konsoli (Mac OS X)\footnote{Istnieją środowiska graficzne do obsługi git, np. podczas instalacji Git for Windows dostajemy program Git GUI, GitHub oraz Bitbucket udostępniają dedykowane programy graficzne, wiele popularnych IDE jest zintegrowanych z gitem (bądź można zainstalować odpowiedni plugin). Jednak nie tym się tutaj zajmujemy.}.
    
    
    \section{Zakładanie repozytorium}
        Nowe repozytorium zakładamy przez interfejs webowy Bitbucketa.
            	    
	    \begin{figure}[hb!]
	        \centering
	        \includegraphics[width=1.0\textwidth]{img/status1.png}
	        \caption{git status - przed git add}
	        \label{Fig:status1}
	    \end{figure}
	    
	    \begin{enumerate}
		    \item Pod opcją ,,Create'' (symbol ,,+'' po lewej stronie) wybieramy ,,Repository''.
		    \item Wybieramy nazwę repozytorium (na wszelki wypadek bez spacji), czy ma być prywatne (na nasze potrzeby w 80\% przypadków lepiej używać prywatnego repo), czy chcemy szablon pliku README. Jako system kontroli wersji wybieramy git.
		    \item Opcje zaawansowane można zostawić tak, jak są.
		    \item Klonujemy założone repozytorium na swój dysk. Tym razem w widoku repozytorium wybieramy ,,Create'', a następnie wybieramy ,,Clone this repository''.
		    \item W otwartym oknie upewniamy się, czy na liście rozwijanej jest HTTPS i kopiujemy komendę w ramce.
		    \item W konsoli zakładamy / przechodzimy do katalogu, w którym chcemy przechowywać repozytorium, wklejamy i wykonujemy skopiowaną komendę.
		    \item Po ewentualnym wpisaniu nazwy użytkownika i hasła program tworzy katalog roboczy o nazwie takiej, jak nazwa repozytorium. Jeżeli repozytorium nie było puste, to od razu kopiuje na lokalny komputer wszystkie pliki i historię wersji.
	    \end{enumerate}
	    
	    
    \section{Praca z repozytoriami}
	    
	    Gdy już mamy gotowe i sklonowane repozytorium, możemy rozpocząć pracę. Wszystkie pliki, których wersje w danym projekcie chcemy śledzić, przechowujemy w katalogu roboczym założonym komendą git clone. Sposób pracy jest taki:
	    
	    \begin{itemize}
	        \item Normalnie pracuje się z plikami, z tym wyjątkiem, że przechowujemy je w katalogu roboczym repozytorium.
	        \item Gdy to jest pożądane, dodaje się bieżący stan pliku (snapshot) do przechowalni.
	        \item Zatwierdza się zmiany (commit), czyli przenosi stan plików z przechowalni do repozytorium.
	        \item Co jakiś czas warto przesłać stan repozytorium na serwer Bitbucketa.
	    \end{itemize}
	    
	    \begin{figure}[hb!]
	        \centering
	        \includegraphics[width=1.0\textwidth]{img/add.png}
	        \caption{git add}
	        \label{Fig:add}
	    \end{figure}
	    
	    Rysunek \ref{Fig:status1} przedstawia sytuację zaraz po sklonowaniu repozytorium. Stworzyłem folderze roboczym jakiś plik. Za pomocą komendy
	    \begin{lstlisting}
	        $ git status
	    \end{lstlisting}
	    możemy sprawdzić, które pliki były zmieniane w czasie, który minął od ostatniego commitu (w tym wypadku od momentu założenia repo) - tutaj jest to ,,f.txt'' (Rys. \ref{Fig:status1}).
	    
	    Gdy przyjdzie czas na zrobienie migawki danego pliku korzystamy z polecenia:
	    \begin{lstlisting}
	        $ git add nazwa_pliku
	    \end{lstlisting}
	    (Rys. \ref{Fig:add}). Jak widzimy, status pliku się zmienił.
	    
	    Teraz można dodać aktualny stan pliku do historii wersji za pomocą polecenia
	    \begin{lstlisting}
	        $ git commit -m 'krotki opis commitu'
	    \end{lstlisting}
	    Zmiana taka będzie przechowywana wyłącznie lokalnie, dopóki nie wgramy jej na serwer poleceniem
	    \begin{lstlisting}
	        $ git push origin master
	    \end{lstlisting}
	    (Rys. \ref{Fig:commit}). Komenda oznacza, że pchamy zmiany do serwera (\textit{origin}, czyli tutaj Bitbucket) do gałęzi (\textit{branch}) master (czyli głównej gałęzi projektu; więcej informacji o gałęziach będzie podane później). Można sprawdzić, że teraz w webowym interfejsie Bitbucketa również widzimy nasz commit.
	    
	    \begin{figure}[h]
	        \centering
	        \includegraphics[width=1.0\textwidth]{img/commit-push.png}
	        \caption{git commit i git push}
	        \label{Fig:commit}
	    \end{figure}
	    
	    Jeżeli pracujemy na innym komputerze, zaktualizujemy jakiś plik i wyślemy nową historię commitów na serwer (lub zrobimy commit na stronie Bitbucketa - to też możliwe) to można za pomocą komendy
	    \begin{lstlisting}
	        $ git pull origin master
	    \end{lstlisting}
	    uaktualnić naszą lokalną kopię repozytorium (a dokładniej branch master).
    

    \section{Specjalne pliki}    
        Jeżeli przejrzymy kilka repozytoriów na GitHubie/Bitbucket (np. \href{https://bitbucket.org/mozdzi/git-tutorial/src}{repozytorium z tym tekstem}), to natkniemy się zazwyczaj na 2 specjalne pliki: ,,README.md'' oraz ,,.gitignore''.
        
        \begin{itemize}
            \item README.md to opis projektu, zwykle pisany w języku Markdown, stąd rozszerzenie .md. Markdown to bardzo prosty język znaczników służący do formatowania tekstu, można poczytać tutaj: \url{https://en.wikipedia.org/wiki/Markdown}.
            \item .gitignore jest plikiem, w którym można umieścić wszystko, co ma być pomijane podczas kolejnych snapshotów i commitów. Przykładowy wygląd podstawowego .gitignore dla plików LaTeX:
            \begin{lstlisting}
                *.aux
                *.log
                *.out
                *.gz
            \end{lstlisting}
        \end{itemize}
        
        \begin{figure}[hb!]
	        \centering
	        \includegraphics[width=0.7\textwidth]{img/branch.png}
	        \caption{2 gałęzie: master i testing oraz wskaźnik HEAD}
	        \label{Fig:branch}
	    \end{figure}   
    
    \section{Gałęzie}
        Omówione powyżej sprawy powinny w zupełności wystarczyć do pracy indywidualnej nad szeroką klasą projektów. Wyobraźmy sobie jednak następującą sytuację: do jednego tekstu w LaTeX-ie poprawki wprowadzają równolegle 2 osoby i każdy chce commitować swoje zmiany, co powoduje, że repozytorium będzie miało 2 różne lokalne wersje, a ponadto po pchnięciu tego na serwer ostatnia wersja będzie wersją z poprawkami tylko jednego użytkownika. Żeby uniknąć takich przykrych sytuacji, git jest wyposażony w mechanizm gałęzi (\textit{branch})\footnote{Oczywiście bardzo przydaje się to też dla projektów jednoosobowych.}. Gałąź można wyobrazić sobie jako nową, niezależną od zmian w równoległych gałęziach, linię rozwoju repozytorium. Jest to realizowane jako wskaźnik do konkretnej migawki, nie jest zakładane żadne nowe repozytorium (Rys. \ref{Fig:branch}) \par     
        
        Przy zakładaniu repozytorium dostajemy gałąź ,,master''. Aby utworzyć nową gałąź, używamy polecenia:
        \begin{lstlisting} 
            $ git branch nazwa_galezi
        \end{lstlisting}
        Jednak to nie powoduje przełączenia do tej gałęzi. O gałęzi, w której aktualnie pracujemy, informuje wskaźnik HEAD (Rys. \ref{Fig:branch}). Aby przesunąć HEAD na nową gałąź, używamy (Rys. \ref{Fig:branch-checkout}):
        \begin{lstlisting} 
            $ git checkout nazwa_galezi
        \end{lstlisting}
        
        \begin{figure}[h]
	        \centering
	        \includegraphics[width=1.0\textwidth]{img/branch-checkout.png}
	        \caption{Założenie gałęzi i przejście do niej}
	        \label{Fig:branch-checkout}
	    \end{figure}
	    
	    \begin{figure}[h!]
	        \centering
	        \includegraphics[width=0.9\textwidth]{img/branches.png}
	        \caption{Zabawa z gałęziami}
	        \label{Fig:branches}
	    \end{figure}
	    
	    Teraz wszystkie commity są rozwijane w dwóch osobnych liniach, zależnie od gałęzi, w której pracujemy. Przełączanie gałęzi powoduje zmianę zawartości katalogu roboczego do stanu z danej gałęzi (Rys. \ref{Fig:branches}). Jednak dobrą praktyką jest wykonywanie commitów przed checkoutem.
	    
	    Po jakimś czasie przychodzi zwykle potrzeba przeniesienia zmian z gałęzi do gałęzi - jak w przykładzie z LaTeX-em, po poprawkach w tekście chcemy, żeby były one widoczne w gałęzi master. W tym celu istnieje mechanizm scalania (\textit{merge}) Należy najpierw przełączyć się do gałęzi, do której chcemy scalić inną gałąź, a następnie wykonaniu polecenie:
	    \begin{lstlisting} 
            $ git merge nazwa_galezi
        \end{lstlisting}
        Niepotrzebną już gałąź usuwamy komendą:
        \begin{lstlisting} 
            $ git branch -d nazwa_galezi
        \end{lstlisting}
        Zobacz Rys. \ref{Fig:merge}.
        
        \begin{figure}[h]
	        \centering
	        \includegraphics[width=1.0\textwidth]{img/merge.png}
	        \caption{Scalanie gałęzi}
	        \label{Fig:merge}
	    \end{figure}
	    
	    Jeżeli w kilku gałęziach pracowaliśmy nad tym samym fragmentem przy scalaniu występuje konflikt. Git wyświetla wtedy komunikat o problemie, a do plików dodaje znaczniki, gdzie występują konflikty, które musimy poprawić już ręcznie. Wygląda to mniej więcej tak, jak na rysunku \ref{Fig:konflikt}. Po poprawieniu konfliktów można uruchomić git add na zmodyfikowanym pliku, co informuje system, że plik jest już ok, i dalej pracować jak zawsze (commit, ewentualnie push, ewentualnie usunięcie niepotrzebnej gałęzi) (Rys. \ref{Fig:konflikt}).
	    
	    \begin{figure}[h]
	        \centering
	        \includegraphics[width=1.0\textwidth]{img/konflikt.png}
	        \caption{Konflikty przy scalaniu}
	        \label{Fig:konflikt}
	    \end{figure}
	    
    
    \section{Inne przydatne rzeczy}
        To oczywiście nie wyczerpuje możliwości gita, ale pozwala w miarę wygodnie z nim pracować. Tutaj umieszczam kilka poleceń, które mogą być przydatne:
        
        \begin{itemize}
            \item \begin{lstlisting} 
                    $ git push --all 
                    $ git pull --all
                  \end{lstlisting}
                  Pozwalają pchnąć albo pociągnąć wszystkie zmiany w repozytorium (wszystkie gałęzie).
            \item \begin{lstlisting} 
                    $ git checkout -b nazwa_galezi
                  \end{lstlisting}
                  Odpowiednik
                  \begin{lstlisting} 
                    $ git branch nazwa_galezi
                    $ git checkout nazwa_galezi
                  \end{lstlisting}
                  czyli tworzy nową gałąź i od razu przenosi do niej wskaźnik HEAD.
            \item \begin{lstlisting} 
                    $ git commit --amend
                  \end{lstlisting}
                  Uzupełnia ostatni commit o dodane do poczekalni (git add) w międzyczasie rzeczy oraz pozwala zmodyfikować notatkę dołączoną do commitu; jeżeli nic nie zostało dodane, to umożliwia tylko modyfikację notatki.
            \item \begin{lstlisting} 
                    $ git reset HEAD nazwa_pliku
                  \end{lstlisting}
                  Usuwa dodany plik z poczekalni.
            \item \begin{lstlisting} 
                    $ git log
                  \end{lstlisting}
                  Wyświetla listę commitów wraz z funkcjami skrótu.
            \item \begin{lstlisting} 
                    $ git checkout <SHA-1>
                  \end{lstlisting}
                  Przywraca chwilowo commit identyfikowany funkcją skrótu SHA-1 (zamiast <SHA-1> można wpisać całość haszu albo kilka pierwszych cyfr, 6-10, wystarczy, żeby dało się jednoznacznie zidentyfikować commit).
            \item \begin{lstlisting} 
                    $ git reset --hard <SHA-1>
                  \end{lstlisting}
                  Przywraca na stałe commit <SHA-1>. Proszę niezwykle uważać, bo zmian takich nie można cofnąć!
        \end{itemize}
        
        
    \section*{Notki końcowe}
        Informacje zawarte w tym tekście nie są ani nawet nie aspirują do miana kompletnego opisu gita. Pewnie wielu przydatnych funkcji nie znam bądź z nich nie korzystam, np. wiem, że gałęzie mają o wiele bardziej zaawansowane opcje, istnieją metody przywracania poprzednich wersji repozytoriów bez całkowitej utraty danych, w serwisach hostingowych istnieje system forków, czyli kopii cudzych repozytoriów, być może też środowiska graficzne są wygodniejsze w użyciu. Niemniej wszystko, co tu opisałem, wykorzystuję na co dzień w swojej pracy i uważam za bardzo przydatne. Po więcej informacji na temat git odsyłam do \href{https://git-scm.com/doc}{oficjalnej dokumentacji}. \par
        Prywatne repozytoria można traktować jak swoisty dropbox dla kodów, tak, by zawsze mieć dostęp do aktualnych wersji napisanych programów czy tekstów, a przy okazji mamy gwarancję, że nie popsujemy nic przez przypadek (mnie się to zdarzyło 2 razy i do dziś nie wiem, jak uruchomić takie programy). \par
        Jeżeli Panowie zdecydują się wykorzystywać git/bitbucket do pracy w grupie, to proszę wysłać mi namiary na swoje konta w Bitbucket - tworzy się wtedy zespół i repozytoria zakłada się dla zespołu. \par
        Wszystkie obrazki pochodzą z podręcznika \href{https://git-scm.com/book/en/v2}{Pro Git} bądź są screenshotami z mojego komputera.
	    
\end{document}
